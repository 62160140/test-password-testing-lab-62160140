// //function ปกติ
// function checkLength(password){
//   return true;
// }

// // !arrow function
// const checkLength  = (password)=>{
//   return true
// }
//  ------------- checkLength -------------
// !เป็น function และเป็นตัวแปรด้วย
const checkLength = function (password) {
  //  *password <=8 return true; else false
  return password.length >= 8 && password.length <= 25
}
//  ------------- checkLength -------------
//  ------------- checkAlphabet -------------
const checkAlphabet = function (password) {
  // !regular expression
  // const alphabets = 'abcdefghijklmnopqrstuvwxyz'
  // for (const ch of password) {
  //   if (alphabets.includes(ch.toLowerCase())) return true
  // }
  // regular expression

  // !iregular expression
  return /[a-z,A-Z]/.test(password)
}
//  ------------- checkAlphabet -------------
//  ------------- checkDigit -------------
const checkDigit = function (password) {
  // !iregular exspression
  return /[1-9]/.test(password)
}
//  ------------- checkDigit -------------
//  ------------- checkSymbol -------------
const checkSymbol = function (password) {
  const symbols = '(!"#$%&()*+,-./:;<=>?@[]^_`{|}~)'
  for (const ch of password) {
    if (symbols.includes(ch)) return true
  }
  return false
}
//  ------------- checkSymbol -------------
//  ------------- checkPassword -------------
const checkPassword = function (password) {
  return checkLength(password) &&
  checkAlphabet(password) &&
  checkDigit(password) &&
  checkSymbol(password)
}
//  ------------- checkPassword -------------

// --------------------install eslint-------------------------
// ?การ reviews คือการที่คนตกลงกันว่าจะเอา code แบบไหน spacing   ยังไง entering ยังไง?
// ?เขาไม่ค่อยมาตกลงกันดังนั่นจึงมีี tool eslint
// todo 0. npm init
// ?1.ติดตั้ง npm install eslint --save-dev
// todo --save = เป็น library
// todo --save-dev = เป็น tool
// ?2. eslint --init กำหนดค่าเริ่มต้นว่าจะให้lintมีบทบาทยังไงใน project
// Javscript module
// CommonJS <--- เราใช้ตัวนี้
// module = คือ ถ้ามี javascript มากกว่า 1 เราจะอ้างอิงถึงยังไง
// ?3. ทำให้ vscode รุ้จัก eslint (โดยการลง extension)
// ?4. ค้นหา config ใน goole : eslint vscode config onsave stackoverflow
// --------------------install eslint-------------------------
// !export
module.exports = {
  checkLength,
  checkAlphabet,
  checkDigit,
  checkSymbol,
  checkPassword
}

// --------------------jest-------------------------
// ?jest,mocha and chai เป็น javascript test
// ?junit เป็น java
// ?1. npm install --save-dev jest
// ?2. ค้นหา extionsion jest ใน vscode
// ?3. สร้าง file test ชื่อ.test.js
// ? ตอนนี้ jest ยังไม่รุ้จัก eslint
// ?4. npm i eslint-plugin-jest
// ?5. https://www.npmjs.com/package/eslint-plugin-jest  ใส่ plugin: และ  env และ rule: ใน eslintrc.js
// --------------------jest-------------------------
//  !เเก้ package.json ตัว test : jest จะได้ไม่ต้องรันด้วย npx เพราะ jest  install แค่ใน package เรา ไม่ได้เป็น global
//  !แก้ package.json ตัว test-coverage : "jest --coverage" เพื่อรันคำสั่ง npm run jest-coverage ไม่ต้อง npx run jest --coverage
// !test suit = descibe
