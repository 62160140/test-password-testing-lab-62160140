// !พิมพ์ desc เพื่อระบุ group
// //ดูตัวอย่างใน docs ของเว็ป jest ค้นหา describe
// ?describe ==> test==> expect

// *อยากได้อะไรจากตัวที่ export มาใส่เข้าไป
const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')

// ---------------------!Test  Password Length!------------------------
describe('Test Password Length', () => {
  // ! testcase : 1 true
  test('should 8 characters to be True', () => {
    // !tb
    expect(checkLength('12345678')).toBe(true)
  })

  // ! testcase : 2 false
  test('should 7 characters to be false', () => {
    // //return false
    expect(checkLength('1234567')).toBe(false)
  })

  // ! testcase : 3 false
  test('should 26 chacracters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })

  // ! testcase : 4 true
  test('should 26 chacracters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })
})
// ---------------------Test  Password Length ------------------------

// ---------------------Test  Alphabet in password ------------------------
describe('Test Alphabet ', () => {
  // ! testcase : 1 true
  test('has alphabet in m password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  // ! testcase : 2 true
  test('has alphabet in a password', () => {
    expect(checkAlphabet('a')).toBe(true)
  })
  // ! testcase : 3 true
  test('has alphabet in Q password', () => {
    expect(checkAlphabet('Q')).toBe(true)
  })
  // ! testcase : 4  false
  test('has not alphabet in password', () => {
    expect(checkAlphabet('111')).toBe(false)
  })
})
// --------------------- end Test  Alphabet in password ------------------------

// ---------------------Test  Symbol in password ------------------------
describe('Test Symbol ', () => {
  // ! testcase : 1 true
  test('has @ in password', () => {
    expect(checkSymbol('@pakkawat')).toBe(true)
  })
  // ! testcase : 2 true
  test('has ! in password', () => {
    expect(checkSymbol('!a')).toBe(true)
  })
  // ! testcase : 3 true
  test('has ` in password', () => {
    expect(checkSymbol('`Q')).toBe(true)
  })
  // ! testcase : 4 true
  test('has ? in password', () => {
    expect(checkSymbol('?111')).toBe(true)
  })
  // ! testcase : 5 false
  test('has not symbol in password', () => {
    expect(checkSymbol('111')).toBe(false)
  })
})
// ---------------------end Test  Symbol in password ------------------------

// ---------------------Test  Digit in password ------------------------

describe('Test digit', () => {
  // ! testcase : 1 true
  test('has alphabet in m password', () => {
    expect(checkDigit('1')).toBe(true)
  })
  // ! testcase : 2 true
  test('has alphabet in a password', () => {
    expect(checkDigit('2')).toBe(true)
  })
  // ! testcase : 3 true
  test('has alphabet in Q password', () => {
    expect(checkDigit('3')).toBe(true)
  })
  // ! testcase : 4  false
  test('has not alphabet in password', () => {
    expect(checkDigit('a')).toBe(false)
  })
})

// ---------------------end Test  Digit in password ------------------------
// ---------------------Test  password in password ------------------------

describe('Test all password rule', () => {
  // ! testcase : 1 true
  test('Test all password rule to be true ', () => {
    expect(checkPassword('@Pakawat01')).toBe(true)
  })

  // ! testcase : 2 false
  test('Test all password rule to be false ', () => {
    expect(checkPassword('Pakawat01')).toBe(false)
  })
})

// ---------------------end test  password in password ------------------------
